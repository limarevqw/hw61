<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Dish;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadDishData extends Fixture implements DependentFixtureInterface
{
    private static $dishes = [];

    public function load(ObjectManager $manager)
    {
        $namesOfDishes = [
            "Манты",
            "Лагман",
            "Гуляш",
            "Шашлык",
            "Плов",
            "Пельмени",
            "Голубцы",
            "Мясо по-итальянски",
            "Самсы",
            "Бифштекс"
        ];
        for ($i = 1; $i <= 10; $i++) {
            $dish = new Dish();
            $dish
                ->setName($namesOfDishes[$i - 1])
                ->setPrice($this->getRandomPrice())
                ->setImg("dish{$i}");

            if($i < 6) {
                $dish->setInstitution($this->getReference(LoadInstitutionData::INSTITUTION_ONE));
            } else {
                $dish->setInstitution($this->getReference(LoadInstitutionData::INSTITUTION_TWO));
            }
            $manager->persist($dish);
            $dishIdent = "dish{$i}";
            $this->addReference($dishIdent, $dish);
            self::$dishes[] = $dishIdent;
        }

        $manager->flush();

    }

    public function getDependencies()
    {
        return array(
            LoadInstitutionData::class
        );
    }

    public function getRandomPrice() {
        return rand(10, 30) * 10;
    }

    public static function getDishes() {
        return self::$dishes;
    }
}