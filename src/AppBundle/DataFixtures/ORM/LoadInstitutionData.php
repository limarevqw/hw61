<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Institution;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class LoadInstitutionData extends Fixture
{
    const INSTITUTION_ONE = 'Ала-Тоо';
    const INSTITUTION_TWO = 'Фаиза';

    public function load(ObjectManager $manager)
    {
        $place1 = new Institution();
        $place1
            ->setName('Ала-Тоо')
            ->setDescription('"Ала-Тоо" — клуб, ресторан, летнее кафе и аквапарк на Южной Магистрали. В меню ресторана, клуба и кафе представлена европейская и национальная кухня')
            ->setImg('alatoo.jpg');

        $manager->persist($place1);

        $place2 = new Institution();
        $place2
            ->setName('Фаиза')
            ->setDescription('"Фаиза" - сеть кафе, заслужившая известность далеко за пределами страны, благодаря превосходной кухне, первоклассному обслуживанию и вниманию к каждому гостю')
            ->setImg('faiza.jpg');

        $manager->persist($place2);
        $manager->flush();

        $this->addReference(self::INSTITUTION_ONE, $place1);
        $this->addReference(self::INSTITUTION_TWO, $place2);
    }
}
