<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Dish;
use AppBundle\Entity\Institution;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class HomeController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $institutions = $this->getDoctrine()
            ->getRepository(Institution::class)
            ->findAll();

        $dishesByInstitutions = [];
        foreach ($institutions as $institution) {
            $dishes = $this->getDoctrine()
                ->getRepository(Dish::class)
                ->getMostPopularDishByInstitution($institution, 2);

            $dishesByInstitutions[] = [
                'institution' => $institution,
                'dishes' => $dishes
            ];
        }

        return $this->render("@App/home/index.html.twig", [
            'dishesByInstitutions' => $dishesByInstitutions
        ]);
    }

    /**
     * @Route("/show/{id}")
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction($id)
    {
        $institutions = $this->getDoctrine()
            ->getRepository(Institution::class)
            ->find($id);


        $dishes = $this->getDoctrine()
            ->getRepository(Dish::class)
            ->getMostDishByInstitution($institutions);

        return $this->render("@App/home/show.html.twig", [
            'institutions' => $institutions,
            'dishes' => $dishes,
        ]);
    }
}